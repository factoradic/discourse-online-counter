import Ember from 'ember';
import {
  ajax
} from 'discourse/lib/ajax'

var inject = Ember.inject;

export default Ember.Service.extend({
  after: 'message-bus',
  messageBus: window.MessageBus,
  count: 0,
  init() {
    ajax('/onlinecounter/get.json', {
      method: 'GET'
    }).then(function (result) {
      this.set('count', result['count']);
      const onlineService = this;
      this.messageBus.subscribe('/online-counter', function (data) {
        onlineService.set('count', data.count);
      })
    }.bind(this), function (msg) {
      console.log(msg);
    });
  }
});