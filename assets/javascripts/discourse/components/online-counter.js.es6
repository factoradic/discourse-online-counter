import Ember from 'ember';
var inject = Ember.inject;

export default Ember.Component.extend({
  showOnlineCounter: function () {
    return this.siteSettings.online_counter_enabled
  }.property(),
  online: inject.service('online-service'),
  count: function () {
    return this.get('online').count
  }.property('online.count')
});