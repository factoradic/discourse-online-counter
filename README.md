# Discourse Online Counter Plugin

A Discourse plugin which displays a number of users currently online.

### [How to Install a Plugin](https://meta.discourse.org/t/install-a-plugin/19157)
