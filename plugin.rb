# name: Online Counter
# about: Discourse plugin displaying number of users online
# version: 0.0.2
# authors: Maciej Wiercioch

enabled_site_setting :online_counter_enabled

PLUGIN_NAME ||= 'online_counter'.freeze

after_initialize do
  module ::OnlineCounter
    class Engine < ::Rails::Engine
      engine_name PLUGIN_NAME
      isolate_namespace OnlineCounter
    end
  end

  require_dependency 'application_controller'

  class OnlineCounter::OnlineCounterController < ::ApplicationController
    def on_request
      respond_to do |format|
        data = User.where("last_seen_at > ?", SiteSetting.online_counter_active_timeago.minutes.ago).count
        format.json { render json: { "count" => data.as_json(:root => false) }.to_json }
      end
    end
  end

  OnlineCounter::Engine.routes.draw do
    get '/get' => 'online_counter#on_request'
  end

  ::Discourse::Application.routes.append do
    mount ::OnlineCounter::Engine, at: '/onlinecounter'
  end

  module ::Jobs
    class OnlineCounterJob < Jobs::Scheduled
        every 1.minutes
        def execute(args)
          return if !SiteSetting.online_counter_enabled?
          data = User.where("last_seen_at > ?", SiteSetting.online_counter_active_timeago.minutes.ago).count
          MessageBus.publish('/online-counter', {'count':data})
        end
      end
  end
end
